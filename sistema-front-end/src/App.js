import React, { Component } from 'react';
import './css/bootstrap.min.css';
import './css/dashboard.css';
import './css/login.css';
import {Link} from 'react-router';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isExpanded: null
        }
    }

    handleToggle(e, id){
        this.setState({
            isExpanded: id
        })
    }

    render() {
        const {isExpanded} = this.state;
        return (
            <div>
            <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
                <Link className="navbar-brand col-sm-3 col-md-2 mr-0" to="/" onClick={(e) => this.handleToggle(e, 0)}>Company name</Link>
                <ul className="navbar-nav px-3">
                    <li className="nav-item text-nowrap">
                    <Link className="nav-link" to="/logout">Logout</Link>
                    </li>
                </ul>
            </nav>
            <div className="container-fluid">
                <div className="row">
                    <nav className="col-md-2 d-none d-md-block bg-light sidebar">
                        <div className="sidebar-sticky">
                        <ul className="nav flex-column">
                            <li className="nav-item">
                            <Link className={`nav-link ${isExpanded===1 ? 'active' : ''}`}  to="administrador" onClick={(e) => this.handleToggle(e, 1)}>
                                <span data-feather="home"></span>
                                Administradores
                            </Link>
                            </li>
                            <li className="nav-item">
                            <Link className={`nav-link ${isExpanded===2 ? 'active' : ''}`}  to="orcamento" onClick={(e) => this.handleToggle(e, 2)}>
                                <span data-feather="bar-chart-2"></span>
                                Orçamentos
                            </Link>
                            </li>
                            <li className="nav-item">
                            <Link className={`nav-link ${isExpanded===3 ? 'active' : ''}`}  to="produto" onClick={(e) => this.handleToggle(e, 3)}>
                                <span data-feather="bar-chart-2"></span>
                                Produtos
                            </Link>
                            </li>
                            <li className="nav-item">
                            <Link className={`nav-link ${isExpanded===4 ? 'active' : ''}`}  to="cliente" onClick={(e) => this.handleToggle(e, 4)}>
                                <span data-feather="bar-chart-2"></span>
                                Clientes
                            </Link>
                            </li>
                            <li className="nav-item">
                            <Link className={`nav-link ${isExpanded===5 ? 'active' : ''}`}  to="relatorios" onClick={(e) => this.handleToggle(e, 5)}>
                                <span data-feather="bar-chart-2"></span>
                                Relatórios
                            </Link>
                            </li>
                        </ul>
                        </div>
                    </nav>

                    <div role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                        {this.props.children}
                    </div>
                </div>
            </div>
            </div>
        );
    }
}

export default App;

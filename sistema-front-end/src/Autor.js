import React, { Component } from 'react';
import $ from 'jquery';
import InputCustomizado from './componentes/InputCustomizado';
import SubmitCustomizado from './componentes/SubmitCustomizado';
import PubSub from 'pubsub-js';
import TratadorErros from './TratadorErros';

export default class Administrador extends Component{
    // Inicio do contrutor
    constructor() {
        super();
        this.state = {Login:'', Senha:'',Email:'',Prenome:'',Sobrenome:'',lista:[],existe:null};
        this.enviaForm = this.enviaForm.bind(this);
        this.atualizaLista = this.atualizaLista.bind(this);
        this.criaAdmin = this.criaAdmin.bind(this);
        this.atualizaAdmin = this.atualizaAdmin.bind(this);
        this.removeAdmin = this.removeAdmin.bind(this);
        this.preencheCampos = this.preencheCampos.bind(this);
    }
    atualizaLista(){
      $.ajax({
          url:"http://localhost:8000/administrador",
          dataType: 'json',
          success:function(resposta){
            this.setState({lista:resposta});
          }.bind(this)
        }
      );
    }

    componentDidMount(){
      this.atualizaLista();
     }


    criaAdmin(){
      const novoAdmin = {
              Login:this.state.Login,
              Senha:this.state.Senha,
              Email:this.state.Email,
              Prenome:this.state.Prenome,
              Sobrenome:this.state.Sobrenome,
    };
    $.ajax({
        url:"http://localhost:8000/administrador",
        contentType:'application/json',
        dataType: 'json',
        type:'post',
        data: JSON.stringify(novoAdmin),
        success:function(novaLista){
            this.atualizaLista();
            this.setState({Login:'', Senha:'',Email:'',Prenome:'',Sobrenome:''});
        }.bind(this),
        error:function(resposta){
            if(resposta.status === 400){
                new TratadorErros().publicaErros(resposta.responseJSON);
            }
        },
        beforeSend: function(){
            PubSub.publish("limpa-erros",{});
        }
    });
    }

    atualizaAdmin(){
      const attAdmin ={
            Senha:this.state.Senha,
            Email:this.state.Email,
            Prenome:this.state.Prenome,
            Sobrenome:this.state.Sobrenome,
    };
    var link = "http://localhost:8000/administrador/"+this.state.Login;
    $.ajax({
      url:link,
      contentType:'application/json',
      dataType: 'json',
      type:'put',
      data: JSON.stringify(attAdmin),
      success:function(novaLista){
          this.atualizaLista();
          this.setState({Login:'', Senha:'',Email:'',Prenome:'',Sobrenome:''});
      }.bind(this),
      error:function(resposta){
          if(resposta.status === 400){
              new TratadorErros().publicaErros(resposta.responseJSON);
          }
      },
      beforeSend: function(){
          PubSub.publish("limpa-erros",{});
      }
  });


    }

    // Inicio das funcoes
    enviaForm(evento){
          evento.preventDefault();
          var link = "http://localhost:8000/administrador/"+this.state.Login;
          //faz uma requisicao ao servidor para verificar se o Login já está cadastrado
          // Caso sim, chama a função de atualizar Admin. Caso não sera criado um novo Admin
            $.ajax({
                url:link,
                dataType: 'json',
                success:function(resposta){
                  this.setState({existe:resposta});
                  if(this.state.existe ==null || this.state.existe ==0){
                    this.criaAdmin();
                  }
                  else{
                    this.atualizaAdmin();
                  }
                }.bind(this)
              }
            );
      }

    removeAdmin(login){
      $.ajax({
          url:"http://localhost:8000/administrador/"+login,
          contentType:'application/json',
          dataType: 'json',
          type:'delete',
          success:function(novaLista){
              this.atualizaLista();
              this.setState({Login:'', Senha:'',Email:'',Prenome:'',Sobrenome:''});
          }.bind(this),
          error:function(resposta){
              if(resposta.status === 400){
                  new TratadorErros().publicaErros(resposta.responseJSON);
              }
          },
          beforeSend: function(){
              PubSub.publish("limpa-erros",{});
          }
      });
    }

    preencheCampos(adm){
      this.setState({
        Login: adm.Login,
        Senha: adm.Senha,
        Email: adm.Email,
        Prenome: adm.Prenome,
        Sobrenome: adm.Sobrenome
      });
    }

    salvaAlteracao(e){
      const nome = e.target.name;
      const valor = e.target.value;
      this.setState({
          [nome]: valor,
      });
    }

    // Geracao de HTML
    render(){
        return(
          <div>
                <div className="header">
                    <h1>Cadastro de administradores</h1>
                </div>
                <div className="content" id="content">
                <div className="pure-form pure-form-aligned">
                    <form className="pure-form pure-form-aligned" onSubmit={this.enviaForm} method="post">
                        <InputCustomizado id="Login" type="text" name="Login" value={this.state.Login} onChange={(event) => this.salvaAlteracao(event)} label="Login"/>
                        <InputCustomizado id="Prenome" type="text" name="Prenome" value={this.state.Prenome} onChange={(event) => this.salvaAlteracao(event)} label="Prenome"/>
                        <InputCustomizado id="Sobrenome" type="text" name="Sobrenome" value={this.state.Sobrenome} onChange={(event) => this.salvaAlteracao(event)} label="Sobrenome"/>
                        <InputCustomizado id="Email" type="email" name="Email" value={this.state.Email} onChange={(event) => this.salvaAlteracao(event)} label="Email"/>
                        <InputCustomizado id="Senha" type="password" name="Senha" value={this.state.Senha} onChange={(event) => this.salvaAlteracao(event)} label="Senha"/>
                        <SubmitCustomizado label="Gravar"/>
                    </form>

                </div>
                    <div>
                        <table className="pure-table">
                            <thead>
                                <tr>
                                    <th>Login</th>
                                    <th>Email</th>
                                    <th>Prenome</th>
                                    <th>Sobrenome</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.lista.map(function(administrador){
                                      return (
                                        <tr style={ {cursor:'pointer'}} key={administrador.Login} onClick={() => { this.preencheCampos(administrador)} }>
                                          <td>{administrador.Login}</td>
                                          <td>{administrador.Email}</td>
                                          <td>{administrador.Prenome}</td>
                                          <td>{administrador.Sobrenome}</td>
                                          <td><button className="pure-button pure-button-primary" type="submit" onClick={() => { this.removeAdmin(administrador.Login) }}>X</button> </td>
                                        </tr>
                                      );
                                    },this)
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        );
    }
}

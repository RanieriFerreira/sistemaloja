import React, { Component} from 'react';
import Chart from 'react-apexcharts'
import $ from 'jquery';

export default class Administrador extends Component {
    // Inicio do contrutor
    constructor() {
        super();
        this.state = {
            Login:'', 
            Senha:'',
            Email:'',
            Prenome:'',
            Sobrenome:'',
            lista:[],
            existe:null, 
            flag:false,
            optionsMixedChart: {
                chart: {
                    id: 'basic-bar',
                    toolbar: {
                        show: false
                    }
                },
                plotOptions: {
                    bar: {
                        columnWidth: '50%',
                        endingShape: 'arrow'
                    }
                },
                stroke: {
                    width: [4, 0, 0],
                },
                xaxis: {
                    categories: ['Orçamento 1', 'Orçamento 2', 'Orçamento 3']
                },
                markers: {
                    size: 6,
                    strokeWidth: 3,
                    fillOpacity: 0,
                    strokeOpacity: 0,
                    hover: {
                        size: 8
                    }
                },
                yaxis: {
                    tickAmount: 5,
                    min: 0
                }
            },
            seriesMixedChart: [{
                name: 'Preço',
                type: 'line',
                data: [30, 40, 25]
            }],
            optionsBar: {
                chart: {
                    stacked: true,
                    stackType: '100%',
                    toolbar: {
                        show: false
                    }
                },
                plotOptions: {
                    bar: {
                        horizontal: true,
                    },
                },
                dataLabels: {
                    dropShadow: {
                        enabled: true
                    }
                },
                stroke: {
                    width: 0,
                },
                xaxis: {
                    categories: ['% de Vendas'],
                    labels: {
                        show: false
                    },
                    axisBorder: {
                        show: false
                    },
                    axisTicks: {
                        show: false
                    }
                },
                fill: {
                    opacity: 1,
                    type: 'gradient',
                    gradient: {
                        shade: 'dark',
                        type: "vertical",
                        shadeIntensity: 0.35,
                        gradientToColors: undefined,
                        inverseColors: false,
                        opacityFrom: 0.85,
                        opacityTo: 0.85,
                        stops: [90, 0, 100]
                    },
                },
                legend: {
                    position: 'bottom',
                    horizontalAlign: 'right',
                }
              },
              seriesBar: [{
                name: 'blue',
                data: [32]
              }, {
                name: 'green',
                data: [41]
              }, {
                name: 'yellow',
                data: [12]
              }, {
                name: 'red',
                data: [65]
              }]
        };
        this.atualizaLista = this.atualizaLista.bind(this);
        this.updateCharts = this.updateCharts.bind(this);
    }
    
    async componentWillMount(){
        await this.atualizaLista();
        this.updateCharts();
    }
    
    salvaAlteracao(e){
        const nome = e.target.name;
        const valor = e.target.value;
        this.setState({
            [nome]: valor,
        });
    }

    async atualizaLista(){
        await $.ajax({
            url:"http://localhost:8000/relatorio",
            contentType:'application/json',
            dataType: 'json',
            type:'get',
            success:function(resposta){
                this.setState({ lista: resposta });
            }.bind(this)
        });
    }

    //Graficos
    updateCharts() {
        const totais = [];
        var renda = 0;
        const newMixedSeries = [];
        const newBarSeries = [];

        this.state.lista.map((s) => {
            totais.push(s.Total);
            renda = renda + s.Total;
            return null;
        })

        newMixedSeries.push({
            data: totais, 
            type: 'line', 
            name: 'Preço'
        })
        
        this.state.lista.map((s) => {
            const array = [Math.floor(s.Total/renda*100)];
            newBarSeries.push({ data: array, name: 'Orçamento '+s.ID_Orçamento})
            return null;
        })
    
        this.setState({
          seriesMixedChart: newMixedSeries,
          seriesBar: newBarSeries
        })
      }

    // Geracao de HTML
    render(){
        return(
            <div>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2">Relatório de vendas</h1>
                </div>
                <div className="row">
                    <div className="col-12">
                        <Chart options={this.state.optionsMixedChart} height={300} series={this.state.seriesMixedChart} type="line" />
                    </div>
                    <div className="col-12">
                        <Chart options={this.state.optionsBar} height={140} series={this.state.seriesBar} type="bar" />
                    </div>
                </div>
            </div>
        );
    }
}

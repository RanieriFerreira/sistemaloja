import React, { Component } from 'react';
import $ from 'jquery';
import InputCustomizado from './componentes/InputCustomizado';
import SubmitCustomizado from './componentes/SubmitCustomizado';
import PubSub from 'pubsub-js';
import TratadorErros from './TratadorErros';

export default class Orcamento extends Component{
    // Inicio do contrutor
    constructor() {
        super();
        this.state = {ID_Orçamento:'',Forma_Pagamento:'', ID_Cliente:'',ID_Produto:'',listaProdutos:[],lista : [], clientes : [], produtos:[],Quantidade:'',possui:[]};
        this.enviaForm = this.enviaForm.bind(this);
        this.salvaAlteracao = this.salvaAlteracao.bind(this);
        this.atualizaLista = this.atualizaLista.bind(this);
        this.procuraCliente = this.procuraCliente.bind(this);
        this.preencheCampos = this.preencheCampos.bind(this);
        this.removeOrcamento = this.removeOrcamento.bind(this);
        this.adicionaProduto = this.adicionaProduto.bind(this);
        this.salvaProdutos = this.salvaProdutos.bind(this);
        this.trazLista = this.trazLista.bind(this);
        this.cancelarEdicao = this.cancelarEdicao.bind(this);
    }

    getID(){
        var max=0;
        for (let i = 0; i < this.state.lista.length; i++) {
            if (this.state.lista[i].ID_Orçamento > max) {
                max=this.state.lista[i].ID_Orçamento;
            }
        }
        return max;
    }

    adicionaProduto(){
        var existe = false;
        // TODO: Verificar se um produto já foi adicionado a lista de produtos
        for (let i = 0; i < this.state.produtos.length; i++) {
            if (Number(this.state.produtos[i].ID_Produto) === Number(this.state.ID_Produto)) {
                const possui ={
                    ID_Produto:parseInt(this.state.ID_Produto,10),
                    Nome: this.state.produtos[i].Nome,
                    Valor: this.state.produtos[i].Valor*this.state.Quantidade,
                    Quantidade:parseInt(this.state.Quantidade,10)
                };
                for (let j=0; j < this.state.possui.length; j++) {
                    if (possui.ID_Produto === this.state.possui[j].ID_Produto) {
                        existe = true;
                    }
                }
                if (existe === false) {
                    this.state.possui.push(possui);
                    console.log("Sucesso: Produto inserido na lista")
                } else {
                    console.log("Erro: Produto ja foi inserido na lista")
                }
            }
        }
        this.setState({ID_Produto:'', Quantidade:''});
    }

    procuraCliente(id){
        for (let i = 0; i < this.state.clientes.length; i++) {
            if (this.state.clientes[i].ID_Cliente === id) {
                var Nome = this.state.clientes[i].Prenome + ' ' +this.state.clientes[i].Sobrenome;
                return Nome;
            }
        }
    }


    removeOrcamento(login){
        $.ajax({
            url:"http://localhost:8000/orcamento/"+login,
            contentType:'application/json',
            dataType: 'json',
            type:'delete',
            success:function(novaLista){
                this.atualizaLista();
                this.setState({ID_Orçamento:'',Forma_Pagamento:'', ID_Cliente:'',listaProdutos:[]});
            }.bind(this),
            error:function(resposta){
                if(resposta.status === 400){
                    new TratadorErros().publicaErros(resposta.responseJSON);
                }
            },
            beforeSend: function(){
                PubSub.publish("limpa-erros",{});
            }
        });
    }

    preencheCampos(adm){
        this.setState({
            ID_Orçamento: adm.ID_Orçamento,
            Forma_Pagamento: adm.Forma_Pagamento,
            ID_Cliente: adm.ID_Cliente
        });
        this.trazLista(adm.ID_Orçamento);
    }

    componentDidMount(){
        this.atualizaLista();
    }

    atualizaLista(){
        $.ajax({
            url:"http://localhost:8000/orcamento",
            dataType: 'json',
            success:function(resposta){
                this.setState({lista:resposta});
            }.bind(this)
        });

        $.ajax({
            url:"http://localhost:8000/cliente",
            dataType: 'json',
            success:function(resposta){
                this.setState({clientes:resposta});
            }.bind(this)
        });
        
        $.ajax({
            url:"http://localhost:8000/produto",
            dataType: 'json',
            success:function(resposta){
                this.setState({produtos:resposta});
            }.bind(this)
        });
    }

    trazLista(id){
        $.ajax({
            url:"http://localhost:8000/possui/"+id,
            dataType: 'json',
            success:function(resposta){
                this.setState({possui:resposta});
                console.log(resposta);
            }.bind(this)
        });
    }


    salvaProdutos(){
        for (let i = 0; i < this.state.possui.length; i++) {
            const novoPossui = {
                ID_Orçamento:this.getID()+1,
                ID_Produto:this.state.possui[i].ID_Produto,
                Quantidade:this.state.possui[i].Quantidade,
            };
            console.log(novoPossui);
            $.ajax({
                url:"http://localhost:8000/possui",
                contentType:'application/json',
                dataType: 'json',
                type:'post',
                data: JSON.stringify(novoPossui),
                success:function(novaLista){
                    console.log("NovaLista: "+ novaLista);
                    this.setState({ID_Orçamento:'',Forma_Pagamento:'', ID_Cliente:'',Quantidade:'',listaProdutos:[],possui:[]});
                    this.atualizaLista();
                }.bind(this),
                error:function(resposta){
                    if(resposta.status === 400){
                        new TratadorErros().publicaErros(resposta.responseJSON);
                    }
                },
                beforeSend: function(){
                    PubSub.publish("limpa-erros",{});
                }
            });
        }
    }
    // Inicio das funcoes
    enviaForm(evento){
        evento.preventDefault();
        const novoOrcamento = {
            ID_Orçamento:this.getID()+1,
            Forma_Pagamento:this.state.Forma_Pagamento,
            ID_Cliente:this.state.ID_Cliente
        };
        $.ajax({
            url:"http://localhost:8000/orcamento",
            contentType:'application/json',
            dataType: 'json',
            type:'post',
            data: JSON.stringify(novoOrcamento),
            success:function(novaLista){
                this.salvaProdutos();
                this.setState({ID_Orçamento:'',Forma_Pagamento:'', ID_Cliente:'',listaProdutos:[]});
                this.atualizaLista();
            }.bind(this),
            error:function(resposta){
                if(resposta.status === 400){
                    new TratadorErros().publicaErros(resposta.responseJSON);
                }
            },
            beforeSend: function(){
                PubSub.publish("limpa-erros",{});
            }
        });
    }

    setTitulo(evento){
        this.setState({titulo:evento.target.value});
    }

    salvaAlteracao(e){
        const Nome = e.target.name;
        const Valor = e.target.value;
        this.setState({
            [Nome]: Valor,
        });
    }

    cancelarEdicao() {
        this.setState ({
            possui:[],
            ID_Orçamento: '',
            Forma_Pagamento: '',
            ID_Cliente: ''
        });
    }
    // Geracao de HTML
    render(){
        return(
            <div>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2">Cadastro de orçamentos</h1>
                </div>

                <div className="form-row mb-4">
                    <div className="col-6">  
                        <label htmlFor="ID_Produto">Produto</label>
                        <select className="form-control" value={this.state.ID_Produto} name="ID_Produto" id="ID_Produto" onChange={(event) => this.salvaAlteracao(event)} >
                            <option value="">Selecione produto</option>
                            {
                                this.state.produtos.map(function(produto){
                                    return <option key={produto.ID_Produto} value={produto.ID_Produto}>{produto.Nome}</option>
                                })
                            }
                        </select>
                    </div>
                    <div className="col-6">
                        <InputCustomizado id="Quantidade" type="number" name="Quantidade" value={this.state.Quantidade} onChange={(event) => this.salvaAlteracao(event)}  label="Quantidade"/>
                    </div>
                    <div className="col-12">
                        <label></label>
                        <input className="btn btn-primary" type="submit" onClick={() => { this.adicionaProduto() }} value="Adicionar Produto"/>
                    </div>
                </div>

                <form onSubmit={this.enviaForm} method="post">
                    <div className="form-row">
                        <div className="col-6">
                            <InputCustomizado id="Forma_Pagamento" type="text" name="Forma_Pagamento" value={this.state.Forma_Pagamento} onChange={(event) => this.salvaAlteracao(event)}  label="Forma de Pagamento"/>
                        </div>
                        <div className="col-6">
                            <label htmlFor="ID_Cliente">Cliente</label>
                            <select className="form-control" value={this.state.ID_Cliente} name="ID_Cliente" id="ID_Cliente" onChange={(event) => this.salvaAlteracao(event)} >
                                <option value="">Selecione cliente</option>
                                {
                                    this.state.clientes.map(function(cliente){
                                        return <option key={cliente.ID_Cliente} value={cliente.ID_Cliente}>{cliente.Prenome} {cliente.Sobrenome}</option>
                                    })
                                }
                            </select>
                        </div>
                        <div className="col-2">
                            <SubmitCustomizado label="Salvar orçamento"/>
                        </div>
                        <div className="col-2">
                            <input id="cancelarBtn" className="btn" type="submit" onClick={() => { this.cancelarEdicao() }} value="Cancelar"/>
                        </div>
                    </div>
                </form>
                
                <div className={`${this.state.possui.length===0 ? 'd-none' : ''}`}>
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h1 className="h2">Produtos</h1>
                    </div>
                    <div className="responsive-table">
                        <table className="table">
                            <thead className="table-secondary">
                                <tr>
                                    <th style={{width: "80%"}}>Produto</th>
                                    <th style={{width: "10%"}}>Quantidade</th>
                                    <th style={{width: "10%"}}>Valor total</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.possui.map(function(produto){
                                        return (
                                            <tr  key={produto.ID_Produto}>
                                            <td>{produto.Nome}</td>
                                            <td>{produto.Quantidade}</td>
                                            <td>{produto.Valor} </td>
                                            </tr>
                                        );
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>

                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2">Orçamentos</h1>
                </div>
                <div className="table-responsive">
                    <table className="table">
                        <thead className="table-secondary">
                            <tr>
                                <th style={{width: "20%"}}>Forma de Pagamento</th>
                                <th style={{width: "65%"}}>Cliente</th>
                                <th style={{width: "15%"}}>Opções</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.lista.map(function(orcamento){
                            return (
                                <tr key={orcamento.ID_Orçamento}>
                                    <td>{orcamento.Forma_Pagamento}</td>
                                    <td>{this.procuraCliente(orcamento.ID_Cliente)}</td>
                                    <td><button className="btn btn-primary btn-sm mr-1" type="submit" onClick={() => { this.preencheCampos(orcamento)} }>Editar</button> 
                                    <button className="btn btn-secondary btn-sm" type="submit" onClick={() => { this.removeOrcamento(orcamento.ID_Orçamento) }}>Deletar</button> </td>
                                </tr>
                            );}, this)
                        }
                        </tbody>
                    </table>
                </div>
            </div>

        );
    }
}

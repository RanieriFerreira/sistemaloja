import React, { Component } from 'react';
import {browserHistory} from 'react-router';
import $ from 'jquery';
import InputCustomizado from './componentes/InputCustomizado';
import SubmitCustomizado from './componentes/SubmitCustomizado';


export default class Login extends Component {
    constructor(){
        super();
        this.state = {Login:'', Senha:'', msg:'',existe:null};
        this.enviaForm = this.enviaForm.bind(this);
    }

    enviaForm(evento){
        evento.preventDefault();
        var link = "http://localhost:8000/administrador/"+this.state.Login;
        //faz uma requisicao ao servidor para verificar se o Login já está cadastrado
        // Caso sim, chama a função de atualizar Admin. Caso não sera criado um novo Admin
        $.ajax({
            url:link,
            dataType: 'json',
            success:function(resposta){
                this.setState({existe:resposta});
                if(resposta.length > 0 && resposta[0].Senha === this.state.Senha){
                    console.log('Usuário válido!');
                    localStorage.setItem('Login',this.state.Login);
                    browserHistory.push('/');
                }   else{
                    console.log('Usuário inválido!');
                    this.setState({msg:'Não foi possível realizar o login'});
                }
            }.bind(this)
            
        });
    }
    

    salvaAlteracao(e){
        const nome = e.target.name;
        const valor = e.target.value;
        this.setState({
            [nome]: valor,
        });
    }

    render(){ 
        return (
            <div class="container">
                <div class="row">
                <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                    <div class="card card-signin my-5">
                    <div class="card-body-login">
                        <h5 class="card-title text-center">Nome da empresa</h5>
                        <form class="form-signin" onSubmit={this.enviaForm} method="post">
                            <div class="form-label-group">
                                <InputCustomizado id="Login" type="text" name="Login" value={this.state.Login} onChange={(event) => this.salvaAlteracao(event)} label="Login"/>
                            </div>

                            <div class="form-label-group">
                                <InputCustomizado id="Senha" type="password" name="Senha" value={this.state.Senha} onChange={(event) => this.salvaAlteracao(event)} label="Senha"/>
                            </div>
                            
                            <SubmitCustomizado label="Login"/>
                        </form>
                        <span>{this.state.msg}</span>
                    </div>
                    </div>
                </div>
                </div>
            </div>            
        );
    }   
}   
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Administrador from './Administrador';
import Cliente from './Cliente';
import Produto from './Produto';
import Orcamento from './Orcamento';
import Relatorios from './Relatorios';
import Login from './Login';
import Logout from './Logout';
import Home from './Home';
import * as serviceWorker from './serviceWorker';
import {Router,Route,browserHistory,IndexRoute} from 'react-router';

function verificaAutentificacao(nextStage, replace) {
  if(localStorage.getItem('Login') === null){
    replace('/login');
  }
}

ReactDOM.render(
    (<Router history={browserHistory}>
      <Route path="/" component={App}>
        <Route path="/administrador" component={Administrador} onEnter={verificaAutentificacao}/>
        <Route path="/cliente" component={Cliente} onEnter={verificaAutentificacao}/>
        <Route path="/produto" component={Produto} onEnter={verificaAutentificacao}/>
        <Route path="/orcamento" component={Orcamento} onEnter={verificaAutentificacao}/>
        <Route path="/relatorios" component={Relatorios} onEnter={verificaAutentificacao}/>
        <Route path="/logout" component={Logout}/>
        <IndexRoute component={Home} onEnter={verificaAutentificacao}/>
      </Route>
      <Route path="/login" component={Login}/>
    </Router>),
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

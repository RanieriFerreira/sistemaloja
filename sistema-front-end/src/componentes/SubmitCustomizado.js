import React, { Component } from 'react';

export default class SubmitCustomizado extends Component{
    render(){
        return(
            <div className="form-group">
                <label></label> 
                <input type="submit" className="btn btn-primary" value={this.props.label}/>
            </div>
        );
    }
}

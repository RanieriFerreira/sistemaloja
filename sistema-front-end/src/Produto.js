import React, { Component } from 'react';
import $ from 'jquery';
import InputCustomizado from './componentes/InputCustomizado';
import SubmitCustomizado from './componentes/SubmitCustomizado';
import PubSub from 'pubsub-js';
import TratadorErros from './TratadorErros';

export default class Produto extends Component{
    // Inicio do contrutor
    constructor() {
        super();
        this.state = {ID_Produto:-1,Nome:'',Descrição:'',Valor:'',lista:[],existe:null, btnGravar:'Gravar', visibilidade:false};
        this.enviaForm = this.enviaForm.bind(this);
        this.atualizaLista = this.atualizaLista.bind(this);
        this.criaProduto = this.criaProduto.bind(this);
        this.atualizaProduto = this.atualizaProduto.bind(this);
        this.removeProduto = this.removeProduto.bind(this);
        this.preencheCampos = this.preencheCampos.bind(this);
        this.getID = this.getID.bind(this);
        this.descartaAlteracoes = this.descartaAlteracoes.bind(this);
    }
    atualizaLista(){
      $.ajax({
          url:"http://localhost:8000/produto",
          dataType: 'json',
          success:function(resposta){
            this.setState({lista:resposta});
          }.bind(this)
        }
      );
    }

    componentDidMount(){
        this.atualizaLista();
    }

    getID(){
        var max=0;
        for (let i = 0; i < this.state.lista.length; i++) {
            if (this.state.lista[i].ID_Produto > max) {
                max=this.state.lista[i].ID_Produto;
            }
        }
        console.log(max);
        return max;
    }

    criaProduto(){
        const novoProduto = {
            ID_Produto:this.getID()+1,
            Nome:this.state.Nome,
            Descrição:this.state.Descrição,
            Valor:parseFloat(this.state.Valor)
        };
        $.ajax({
            url:"http://localhost:8000/produto",
            contentType:'application/json',
            dataType: 'json',
            type:'post',
            data: JSON.stringify(novoProduto),
            success:function(novaLista){
                console.log(novoProduto);
                this.atualizaLista();
                this.setState({ID_Produto:-1,Valor:'',Nome:'',Descrição:''});
            }.bind(this),
            error:function(resposta){
                if(resposta.status === 400){
                    new TratadorErros().publicaErros(resposta.responseJSON);
                }
            },
            beforeSend: function(){
                PubSub.publish("limpa-erros",{});
            }
        });
    }

    atualizaProduto(){
      const attProduto ={
            Nome:this.state.Nome,
            Descrição:this.state.Descrição,
            Valor:parseFloat(this.state.Valor)
        };
        var link = "http://localhost:8000/produto/"+this.state.ID_Produto;
        $.ajax({
        url:link,
        contentType:'application/json',
        dataType: 'json',
        type:'put',
        data: JSON.stringify(attProduto),
        success:function(novaLista){
            console.log(attProduto);
            this.atualizaLista();
            this.descartaAlteracoes();
        }.bind(this),
        error:function(resposta){
            if(resposta.status === 400){
                new TratadorErros().publicaErros(resposta.responseJSON);
            }
        },
        beforeSend: function(){
            PubSub.publish("limpa-erros",{});
        }
    });


    }

    // Inicio das funcoes
    enviaForm(evento){
          evento.preventDefault();
          if(this.state.ID_Produto === -1){
            this.criaProduto();
          }
          else{
            this.atualizaProduto();
          }
      }

    removeProduto(id){
      console.log("http://localhost:8000/produto/"+id);
      $.ajax({
          url:"http://localhost:8000/produto/"+id,
          contentType:'application/json',
          dataType: 'json',
          type:'delete',
          success:function(resposta){
            console.log("sucesso");
              this.atualizaLista();
              this.setState({ID_Produto:-1,
              Valor:'',
              Nome:'',
              Descrição:'',
              btnGravar:'Gravar',
              visibilidade:false});
          }.bind(this),
          error:function(resposta){
            console.log(resposta);
              if(resposta.status === 400){
                  new TratadorErros().publicaErros(resposta.responseJSON);
              }
          },
          beforeSend: function(){
              PubSub.publish("limpa-erros",{});
          }
      });
    }

    preencheCampos(produto){
        this.setState({
            ID_Produto: produto.ID_Produto,
            Nome: produto.Nome,
            Descrição: produto.Descrição,
            Valor: produto.Valor,
            btnGravar:'Atualizar',
            visibilidade:true
        });
    }

    salvaAlteracao(e){
        const nome = e.target.name;
        const valor = e.target.value;
        this.setState({
            [nome]: valor,
        });
    }

    descartaAlteracoes(){
      this.setState({
          ID_Produto:-1,
          Valor:'',
          Nome:'',
          Descrição:'',
          btnGravar:'Gravar',
          visibilidade:false
      });
    }
    // Geracao de HTML
    render(){
        return(
            <div>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2">Cadastro de produtos</h1>
                </div>
                <form onSubmit={this.enviaForm} method="post">
                    <div className="form-row">
                        <div className="col-9">
                            <InputCustomizado id="Nome" type="text" name="Nome" placeholder="Ex: Notebook" value={this.state.Nome} onChange={(event) => this.salvaAlteracao(event)} label="Nome"/>
                        </div>
                        <div className="col-3">
                            <InputCustomizado id="Valor" type="number" name="Valor" placeholder="XX,XX" value={this.state.Valor} onChange={(event) => this.salvaAlteracao(event)} label="Valor"/>
                        </div>
                        <div className="col-12">
                            <InputCustomizado id="Descrição" type="text" name="Descrição" placeholder="Ex: Notebook prata" value={this.state.Descrição} onChange={(event) => this.salvaAlteracao(event)} label="Descrição"/>
                        </div>
                        <div className="col form-group">
                            <SubmitCustomizado label="Gravar"/>
                        </div>    
                    </div>
                </form>
                {this.state.visibilidade && (
                      <form className="pure-form pure-form-aligned" onSubmit={this.descartaAlteracoes}>
                       <SubmitCustomizado  label="Descartar Alterações"/>
                       </form>
                )}
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2">Listagem</h1>
                </div>
                <div className="table-responsive">
                    <table className="table ">
                        <thead className="table-secondary">
                            <tr>
                                <th style={{width: "25%"}}>Nome</th>
                                <th style={{width: "25%"}}>Valor</th>
                                <th style={{width: "25%"}}>Descrição</th>
                                <th style={{width: "25%"}}>Opções</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.lista.map(function(produto){
                                    return (
                                        <tr key={produto.ID_Produto}>
                                            <td>{produto.Nome}</td>
                                            <td>{produto.Valor}</td>
                                            <td>{produto.Descrição}</td>
                                            <td><button className="btn btn-primary btn-sm mr-1" type="submit" onClick={() => { this.preencheCampos(produto)} }>Editar</button> 
                                            <button className="btn btn-secondary btn-sm" type="submit" onClick={() => { this.removeProduto(produto.ID_Produto) }}>Deletar</button> </td>
                                        </tr>
                                    );
                                },this)
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

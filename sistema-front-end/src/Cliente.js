import React, { Component } from 'react';
import $ from 'jquery';
import InputCustomizado from './componentes/InputCustomizado';
import SubmitCustomizado from './componentes/SubmitCustomizado';
import PubSub from 'pubsub-js';
import TratadorErros from './TratadorErros';

export default class Cliente extends Component{
    // Inicio do contrutor
    constructor() {
        super();
        this.state = {ID_Cliente:'', CPF:'',E_mail:'',Prenome:'',Sobrenome:'',lista:[],existe:null, btnGravar:'Gravar', visibilidade:false};
        this.enviaForm = this.enviaForm.bind(this);
        this.atualizaLista = this.atualizaLista.bind(this);
        this.criaCliente = this.criaCliente.bind(this);
        this.atualizaCliente = this.atualizaCliente.bind(this);
        this.removeCliente = this.removeCliente.bind(this);
        this.preencheCampos = this.preencheCampos.bind(this);
        this.getID = this.getID.bind(this);
        this.descartaAlteracoes = this.descartaAlteracoes.bind(this);
    }
    atualizaLista(){
      $.ajax({
          url:"http://localhost:8000/cliente",
          dataType: 'json',
          success:function(resposta){
            this.setState({lista:resposta});
          }.bind(this)
        }
      );
    }

    componentDidMount(){
      this.atualizaLista();
     }

     getID(){
       var max=1;
       for (let i = 0; i < this.state.lista.length; i++) {
        if (this.state.lista[i].ID_Cliente > max) {
          max=this.state.lista[i].ID_Cliente;
        }
      }
      return max;
     }

    criaCliente(){
        const novoCliente = {
            ID_Cliente:this.getID()+1,
            CPF:this.state.CPF,
            E_mail:this.state.E_mail,
            Prenome:this.state.Prenome,
            Sobrenome:this.state.Sobrenome,
        };
        $.ajax({
            url:"http://localhost:8000/cliente",
            contentType:'application/json',
            dataType: 'json',
            type:'post',
            data: JSON.stringify(novoCliente),
            success:function(novaLista){
                console.log(novaLista);
                console.log(novoCliente);
                this.atualizaLista();
                this.setState({ID_Cliente:'', CPF:'',E_mail:'',Prenome:'',Sobrenome:''});
            }.bind(this),
            error:function(resposta){
                if(resposta.status === 400){
                    new TratadorErros().publicaErros(resposta.responseJSON);
                }
            },
            beforeSend: function(){
                PubSub.publish("limpa-erros",{});
            }
        });
    }

    atualizaCliente(){
      const attCliente ={
            Prenome:this.state.Prenome,
            Sobrenome:this.state.Sobrenome,
            CPF:this.state.CPF,
            E_mail:this.state.E_mail,
        };
        var link = "http://localhost:8000/cliente/"+this.state.ID_Cliente;
        $.ajax({
        url:link,
        contentType:'application/json',
        dataType: 'json',
        type:'put',
        data: JSON.stringify(attCliente),
        success:function(novaLista){
            this.atualizaLista();
            this.descartaAlteracoes();
        }.bind(this),
        error:function(resposta){
            if(resposta.status === 400){
                new TratadorErros().publicaErros(resposta.responseJSON);
            }
        },
        beforeSend: function(){
            PubSub.publish("limpa-erros",{});
        }
    });


    }

    // Inicio das funcoes
    enviaForm(evento){
          evento.preventDefault();
          if(this.state.ID_Cliente === ''){
            this.criaCliente();
          }
          else{
            this.atualizaCliente();
          }
      }

    removeCliente(login){
      $.ajax({
          url:"http://localhost:8000/cliente/"+login,
          contentType:'application/json',
          dataType: 'json',
          type:'delete',
          success:function(novaLista){
              this.atualizaLista();
              this.setState({ID_Cliente:'',
              CPF:'',
              E_mail:'',
              Prenome:'',
              Sobrenome:'',
              btnGravar:'Gravar',
              visibilidade:false});
          }.bind(this),
          error:function(resposta){
              if(resposta.status === 400){
                  new TratadorErros().publicaErros(resposta.responseJSON);
              }
          },
          beforeSend: function(){
              PubSub.publish("limpa-erros",{});
          }
      });
    }

    preencheCampos(cliente){
        this.setState({
            ID_Cliente: cliente.ID_Cliente,
            Prenome: cliente.Prenome,
            Sobrenome: cliente.Sobrenome,
            E_mail: cliente.E_mail,
            CPF: cliente.CPF,
            btnGravar:'Atualizar',
            visibilidade:true
        });
        console.log(cliente);
    }

    salvaAlteracao(e){
      const nome = e.target.name;
      const valor = e.target.value;
      this.setState({
          [nome]: valor,
      });
    }

    descartaAlteracoes(){
      this.setState({
          ID_Cliente:'',
          CPF:'',
          E_mail:'',
          Prenome:'',
          Sobrenome:'',
          btnGravar:'Gravar',
          visibilidade:false
      });
    }
    // Geracao de HTML
    render(){
        return(
            <div>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2">Cadastro de clientes</h1>
                </div>
                <form onSubmit={this.enviaForm} method="post">
                    <div className="form-row">
                        <div className="col-6">
                            <InputCustomizado id="Prenome" type="text" name="Prenome" placeholder="Ex: João" value={this.state.Prenome} onChange={(event) => this.salvaAlteracao(event)} label="Prenome"/>
                        </div>
                        
                        <div className="col-6">
                            <InputCustomizado id="Sobrenome" type="text" name="Sobrenome" placeholder="Ex: Silva" value={this.state.Sobrenome} onChange={(event) => this.salvaAlteracao(event)} label="Sobrenome"/>
                        </div>

                        <div className="col-6">
                            <InputCustomizado id="CPF" type="number" name="CPF" placeholder="XXX.XXX.XXX-XX" value={this.state.CPF} onChange={(event) => this.salvaAlteracao(event)} label="CPF"/>
                        </div>

                        <div className="col-6">
                            <InputCustomizado id="E_mail" type="email" name="E_mail" placeholder="Ex: joao12@email.com" value={this.state.E_mail} onChange={(event) => this.salvaAlteracao(event)} label="Email"/>
                        </div>
                        <div className="col form-group">
                            <SubmitCustomizado label="Gravar"/>
                        </div>    
                    </div>
                </form>
                {this.state.visibilidade && (
                    <form onSubmit={this.descartaAlteracoes}>
                        <SubmitCustomizado  label="Descartar Alterações"/>
                    </form>
                )}
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2">Listagem</h1>
                </div>
                <div className="table-responsive">
                    <table className="table ">
                        <thead className="table-secondary">
                            <tr>
                                <th style={{width: "20%"}}>Prenome</th>
                                <th style={{width: "20%"}}>Sobrenome</th>
                                <th style={{width: "20%"}}>E-mail</th>
                                <th style={{width: "20%"}}>CPF</th>
                                <th style={{width: "20%"}}>Opções</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.lista.map(function(cliente){
                                    return (
                                        <tr key={cliente.ID_Cliente}>
                                            <td>{cliente.Prenome}</td>
                                            <td>{cliente.Sobrenome}</td>
                                            <td>{cliente.E_mail}</td>
                                            <td>{cliente.CPF}</td>
                                            <td><button className="btn btn-primary btn-sm mr-1" type="submit" onClick={() => { this.preencheCampos(cliente)} }>Editar</button> 
                                            <button className="btn btn-secondary btn-sm" type="submit" onClick={() => { this.removeCliente(cliente.ID_Cliente) }}>Deletar</button> </td>
                                        </tr>
                                    );
                                },this)
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

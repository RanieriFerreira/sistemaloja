import React, { Component } from 'react';
import $ from 'jquery';
import InputCustomizado from './componentes/InputCustomizado';
import SubmitCustomizado from './componentes/SubmitCustomizado';
import PubSub from 'pubsub-js';
import TratadorErros from './TratadorErros';

export default class Administrador extends Component {
    // Inicio do contrutor
    constructor() {
        super();
        this.state = {Login:'', Senha:'',Email:'',Prenome:'',Sobrenome:'',lista:[]};
        this.enviaForm = this.enviaForm.bind(this);
        this.atualizaLista = this.atualizaLista.bind(this);
        this.criaAdmin = this.criaAdmin.bind(this);
        this.atualizaAdmin = this.atualizaAdmin.bind(this);
        this.removeAdmin = this.removeAdmin.bind(this);
        this.preencheCampos = this.preencheCampos.bind(this);
    }

    atualizaLista(){
        $.ajax({
            url:"http://localhost:8000/administrador",
            dataType: 'json',
            success:function(resposta){
                this.setState({lista:resposta});
            }.bind(this)
        });
    }

    componentDidMount(){
        this.atualizaLista();
    }

    criaAdmin(){
        const novoAdmin = {
            Login:this.state.Login,
            Senha:this.state.Senha,
            Email:this.state.Email,
            Prenome:this.state.Prenome,
            Sobrenome:this.state.Sobrenome,
        };
        $.ajax({
            url:"http://localhost:8000/administrador",
            contentType:'application/json',
            dataType: 'json',
            type:'post',
            data: JSON.stringify(novoAdmin),
            success:function(novaLista){
                this.atualizaLista();
                this.setState({Login:'', Senha:'',Email:'',Prenome:'',Sobrenome:''});
            }.bind(this),
            error:function(resposta){
                if(resposta.status === 400){
                    new TratadorErros().publicaErros(resposta.responseJSON);
                }
            },
            beforeSend: function(){
                PubSub.publish("limpa-erros",{});
            }
        });
    }

    atualizaAdmin(){
        const attAdmin ={
            Senha:this.state.Senha,
            Email:this.state.Email,
            Prenome:this.state.Prenome,
            Sobrenome:this.state.Sobrenome,
        };
        var link = "http://localhost:8000/administrador/"+this.state.Login;
        $.ajax({
            url:link,
            contentType:'application/json',
            dataType: 'json',
            type:'put',
            data: JSON.stringify(attAdmin),
            success:function(novaLista){
                this.atualizaLista();
                this.setState({Login:'', Senha:'',Email:'',Prenome:'',Sobrenome:''});
            }.bind(this),
            error:function(resposta){
                if(resposta.status === 400){
                    new TratadorErros().publicaErros(resposta.responseJSON);
                }
            },
            beforeSend: function(){
                PubSub.publish("limpa-erros",{});
            }
        });
    }

    // Inicio das funcoes
    enviaForm(evento){
        evento.preventDefault();
        var link = "http://localhost:8000/administrador/"+this.state.Login;
        //faz uma requisicao ao servidor para verificar se o Login já está cadastrado
        // Caso sim, chama a função de atualizar Admin. Caso não sera criado um novo Admin
        $.ajax({
            url:link,
            dataType: 'json',
            success:function(resposta){
                if(!resposta.length){
                    this.criaAdmin();
                }
                else{
                    this.atualizaAdmin();
                }
            }.bind(this),
            error:function(resposta){
                if(resposta.status === 400){
                    new TratadorErros().publicaErros(resposta.responseJSON);
                }
            },
            beforeSend: function(){
                PubSub.publish("limpa-erros",{});
            } 
            }
        );
    }

    removeAdmin(login){
        $.ajax({
            url:"http://localhost:8000/administrador/"+login,
            contentType:'application/json',
            dataType: 'json',
            type:'delete',
            success:function(novaLista){
                this.atualizaLista();
                this.setState({Login:'', Senha:'',Email:'',Prenome:'',Sobrenome:''});
            }.bind(this),
            error:function(resposta){
                if(resposta.status === 400){
                    new TratadorErros().publicaErros(resposta.responseJSON);
                }
            },
            beforeSend: function(){
                PubSub.publish("limpa-erros",{});
            }
        });
    }

    preencheCampos(adm){
        this.setState({
            Login: adm.Login,
            Senha: adm.Senha,
            Email: adm.Email,
            Prenome: adm.Prenome,
            Sobrenome: adm.Sobrenome
        });
    }

    salvaAlteracao(e){
        const nome = e.target.name;
        const valor = e.target.value;
        this.setState({
            [nome]: valor,
        });
    }

    // Geracao de HTML
    render(){
        return(
            <div>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2">Novo cadastro</h1>
                </div>
                <form onSubmit={this.enviaForm} method="post">
                    <div className="form-row">
                        <div className="col-6">
                            <InputCustomizado id="Prenome" type="text" name="Prenome" placeholder="Prenome" value={this.state.Prenome} onChange={(event) => this.salvaAlteracao(event)} label="Prenome"/>
                        </div>
                        <div className="col-6">
                            <InputCustomizado id="Sobrenome" type="text" name="Sobrenome" placeholder="Sobrenome" value={this.state.Sobrenome} onChange={(event) => this.salvaAlteracao(event)} label="Sobrenome"/>
                        </div>
                        <div className="col-4">
                            <InputCustomizado id="Login" type="text" name="Login" placeholder="Login" value={this.state.Login} onChange={(event) => this.salvaAlteracao(event)} label="Login"/>
                        </div>
                        <div className="col-4">
                            <InputCustomizado id="Email" type="email" name="Email" placeholder="E-mail" value={this.state.Email} onChange={(event) => this.salvaAlteracao(event)} label="Email"/>
                        </div>
                        <div className="col-4">
                            <InputCustomizado id="Senha" type="password" name="Senha" placeholder="Senha" value={this.state.Senha} onChange={(event) => this.salvaAlteracao(event)} label="Senha"/>
                        </div>
                        <div className="col form-group">
                            <SubmitCustomizado label="Gravar"/>
                        </div>
                    </div>
                </form>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2">Listagem</h1>
                    <div className="btn-toolbar mb-2 mb-md-0">
                    <div className="btn-group mr-2">
                        <button className="btn btn-sm btn-outline-secondary">Share</button>
                        <button className="btn btn-sm btn-outline-secondary">Export</button>
                    </div>
                    </div>
                </div>
                <div className="table-responsive">
                    <table className="table ">
                        <thead className="table-secondary">
                            <tr>
                                <th style={{width: "20%"}}>Prenome</th>
                                <th style={{width: "20%"}}>Sobrenome</th>
                                <th style={{width: "20%"}}>Login</th>
                                <th style={{width: "25%"}}>Email</th>
                                <th style={{width: "15%"}}>Opções</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.lista.map(function(administrador){
                                    return (
                                        <tr key={administrador.Login}>
                                            <td>{administrador.Prenome}</td>
                                            <td>{administrador.Sobrenome}</td>
                                            <td>{administrador.Login}</td>
                                            <td>{administrador.Email}</td>
                                            <td><button className="btn btn-primary btn-sm mr-1" type="submit" onClick={() => { this.preencheCampos(administrador)} }>Editar</button> 
                                            <button className="btn btn-secondary btn-sm" type="submit" onClick={() => { this.removeAdmin(administrador.Login) }}>Deletar</button> </td>
                                        </tr>
                                    );
                                },this)
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

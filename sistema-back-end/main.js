const express = require('express');
const app = express();         
const bodyParser = require('body-parser');
const port = 8000;
var router = express.Router();
var admin = require('./routes/adminRoute');
var orcamento = require('./routes/orcamentoRoute');
var relatorio = require('./routes/relatorioRoute');
var empresa = require('./routes/empresaRoute');
var produto = require('./routes/produtoRoute');
var cliente = require('./routes/clienteRoute');
var conta = require('./routes/contaRoute');
var orcaberto = require('./routes/orcamentoAbertoRoute');
var orcfechado = require('./routes/orcamentoFechadoRoute');
var possui = require('./routes/possuiRoute');

app.use(function (req, res, next) {
    //Enabling CORS 
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
});
//configurando o body parser para pegar POSTS mais tarde
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/', router);
app.use('/administrador',admin);
app.use('/orcamento',orcamento);
app.use('/relatorio',relatorio);
app.use('/empresa',empresa);
app.use('/produto',produto);
app.use('/cliente',cliente);
app.use('/conta',conta);
app.use('/orcaberto',orcaberto);
app.use('/orcfechado',orcfechado);
app.use('/possui',possui);

//inicia o servidor
app.listen(port);
var express = require('express');
var router = express.Router();
var Conta = require('../model/conta');

router.get('/:id?', function (req, res, next) {
    if (req.params.id) {
        Conta.getContaById(req.params.id, function (err, rows) {
            if (err)
            {
                res.json(err);
            } else {
                res.json(rows);
            }
        });
    } else {
        Conta.getAllContas(function (err, rows) {
            if (err)
            {
                res.json(err);
            } else
            {
                res.json(rows);
            }
        });
    }
});
router.post('/', function (req, res, next) {

    Conta.addConta(req.body, function (err, count) {
        if (err)
        {
            res.json(err);
        } else {
            res.json(req.body);//or return count for 1 &amp;amp;amp; 0
        }
    });
});
router.delete('/:id', function (req, res, next) {

    Conta.deleteConta(req.params.id, function (err, count) {

        if (err)
        {
            res.json(err);
        } else
        {
            res.json(count);
        }

    });
});
router.put('/:id', function (req, res, next) {

    Conta.updateConta(req.params.id, req.body, function (err, rows) {

        if (err)
        {
            res.json(err);
        } else
        {
            res.json(rows);
        }
    });
});
module.exports = router;
var express = require('express');
var router = express.Router();
var Possui = require('../model/possui');

router.get('/:id?', function (req, res, next) {
    if (req.params.id) {
        Possui.getPossuiById(req.params.id, function (err, rows) {
            if (err)
            {
                res.json(err);
            } else {
                res.json(rows);
            }
        });
    } else {
        Possui.getAllPossui(function (err, rows) {
            if (err)
            {
                res.json(err);
            } else
            {
                res.json(rows);
            }
        });
    }
});
router.post('/', function (req, res, next) {

    Possui.addPossui(req.body, function (err, count) {
        if (err)
        {
            res.json(err);
        } else {
            res.json(req.body);//or return count for 1 &amp;amp;amp; 0
        }
    });
});
router.delete('/:id', function (req, res, next) {

    Possui.deletePossui(req.params.id, function (err, count) {

        if (err)
        {
            res.json(err);
        } else
        {
            res.json(count);
        }

    });
});

module.exports = router;
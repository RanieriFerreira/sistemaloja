var express = require('express');
var router = express.Router();
var Empresa = require('../model/empresa');

router.get('/:id?', function (req, res, next) {
    if (req.params.id) {
        Empresa.getEmpresaById(req.params.id, function (err, rows) {
            if (err)
            {
                res.json(err);
            } else {
                res.json(rows);
            }
        });
    } else {

        Empresa.getAllEmpresas(function (err, rows) {

            if (err)
            {
                res.json(err);
            } else
            {
                res.json(rows);
            }

        });
    }
});
router.post('/', function (req, res, next) {

    Empresa.addEmpresa(req.body, function (err, count) {
        if (err)
        {
            res.json(err);
        } else {
            res.json(req.body);//or return count for 1 &amp;amp;amp; 0
        }
    });
});
router.delete('/:id', function (req, res, next) {

    Empresa.deleteEmpresa(req.params.id, function (err, count) {

        if (err)
        {
            res.json(err);
        } else
        {
            res.json(count);
        }

    });
});
router.put('/:id', function (req, res, next) {

    Empresa.updateEmpresa(req.params.id, req.body, function (err, rows) {

        if (err)
        {
            res.json(err);
        } else
        {
            res.json(rows);
        }
    });
});
module.exports = router;
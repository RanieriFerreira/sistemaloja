var express = require('express');
var router = express.Router();
var Orcamento = require('../model/orcamentoaberto');

router.get('/', function (req, res, next) {
   
        Orcamento.getAllOrcamentoAbertos(function (err, rows) {
            if (err)
            {
                res.json(err);
            } else
            {
                res.json(rows);
            }
        });
});
router.post('/', function (req, res, next) {

    Orcamento.addOrcamentoAberto(req.body, function (err, count) {
        if (err)
        {
            res.json(err);
        } else {
            res.json(req.body);//or return count for 1 &amp;amp;amp; 0
        }
    });
});
router.delete('/:id', function (req, res, next) {

    Orcamento.deleteOrcamentoAberto(req.params.id, function (err, count) {

        if (err)
        {
            res.json(err);
        } else
        {
            res.json(count);
        }

    });
});
module.exports = router;
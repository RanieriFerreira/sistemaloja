var express = require('express');
var router = express.Router();
var db=require('./db'); //reference of dbconnection.js
 
var Produto={
 
getAllProdutos:function(callback){
 
return db.query("Select * from Produto",callback);
 
},
 getProdutoById:function(id,callback){
 
return db.query("select * from Produto where ID_Produto=?",[id],callback);
 },
 addProduto:function(Produto,callback){
 return db.query("Insert into Produto (ID_Produto, Nome, Descrição, Valor) values (?,?,?,?)",[Produto.ID_Produto,Produto.Nome,Produto.Descrição,Produto.Valor],callback);
 },
 deleteProduto:function(id,callback){
  return db.query("delete from Produto where ID_Produto=?",[id],callback);
 },
 updateProduto:function(id,Produto,callback){
  return db.query("update Produto set Nome=?,Descrição=?,Valor=? where ID_Produto=?",[Produto.Nome,Produto.Descrição,Produto.Valor,id],callback);
 }
 
};
 module.exports=Produto;
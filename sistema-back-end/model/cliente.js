var express = require('express');
var router = express.Router();
var db=require('./db'); //reference of dbconnection.js
 
var Cliente={
 
getAllClientes:function(callback){
 
return db.query("Select * from Cliente",callback);
 
},
 getClienteById:function(id,callback){
 
return db.query("select * from Cliente where ID_Cliente=?",[id],callback);
 },
 addCliente:function(Cliente,callback){
 return db.query("Insert into Cliente values(?,?,?,?,?)",[Cliente.ID_Cliente,Cliente.Prenome,Cliente.Sobrenome,Cliente.CPF,Cliente.E_mail],callback);
 },
 deleteCliente:function(id,callback){
  return db.query("delete from Cliente where ID_Cliente=?",[id],callback);
 },
 updateCliente:function(id,Cliente,callback){
  return db.query("update Cliente set Prenome=?,Sobrenome=?,CPF=?,E_mail=? where ID_Cliente=?",[Cliente.Prenome,Cliente.Sobrenome,Cliente.CPF,Cliente.E_mail,id],callback);
 }
 
};
 module.exports=Cliente;
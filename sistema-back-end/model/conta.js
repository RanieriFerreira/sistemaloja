var express = require('express');
var router = express.Router();
var db=require('./db'); //reference of dbconnection.js
 
var Conta={
 
getAllContas:function(callback){
 
return db.query("Select * from Conta",callback);
 
},
 getContaById:function(id,callback){
 
return db.query("select * from Conta where ID_Num=?",[id],callback);
 },
 addConta:function(Conta,callback){
 return db.query("Insert into Conta values(?,?,?,?)",[Conta.ID_Num,Conta.Tipo_conta,Conta.CNPJ,Conta.ID_Orçamento],callback);
 },
 deleteConta:function(id,callback){
  return db.query("delete from Conta where ID_Num=?",[id],callback);
 },
 updateConta:function(id,Conta,callback){
  return db.query("update Conta set Tipo_conta=?,CNPJ=?,ID_Orçamento=? where ID_Num=?",[Conta.Tipo_conta,Conta.CNPJ,Conta.ID_Orçamento,id],callback);
 }
 
};
 module.exports=Conta;
var express = require('express');
var router = express.Router();
var db=require('./db'); //reference of dbconnection.js
 
var Possui={
 
getAllPossui:function(callback){
 
return db.query("Select * from Possui",callback);
 
},
 getPossuiById:function(id,callback){
 
return db.query("select PO.ID_Orçamento, PO.ID_Produto, PO.Quantidade, PR.Nome, PR.Valor from possui as PO,produto as PR where PO.ID_Produto = PR.ID_Produto and PO.ID_Orçamento=?",[id],callback);
 },
 addPossui:function(Possui,callback){
 return db.query("Insert into Possui (ID_Orçamento, ID_Produto, Quantidade) values(?,?,?)",[Possui.ID_Orçamento,Possui.ID_Produto,Possui.Quantidade],callback);
 },
 deletePossui:function(id,callback){
  return db.query("delete from Possui where ID_Orçamento=?",[id],callback);
 }
 
};
 module.exports=Possui;
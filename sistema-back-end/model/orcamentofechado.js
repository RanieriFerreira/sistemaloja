var express = require('express');
var router = express.Router();
var db=require('./db'); //reference of dbconnection.js
 
var Orcamento={
 
getAllOrcamentosFechados:function(callback){ 
return db.query("Select * from Orçamento_Fecha",callback); 
},
 addOrcamentoFechado:function(Orcamento,callback){
 return db.query("Insert into Orçamento_Fecha values(?)",[Orçamento_Fecha.Data_Venda,Orçamento_Fecha.ID_Orçamento],callback);
 },
 deleteOrcamentoFechado:function(id,callback){
  return db.query("delete from Orçamento_Fecha where ID_Orçamento=?",[id],callback);
 }
 
};
 module.exports=Orcamento;
var express = require('express');
var router = express.Router();
var db=require('./db'); //reference of dbconnection.js
 
var Orcamento={
 
getAllOrcamentos:function(callback){
 
return db.query("Select * from Orçamento",callback);
 
},
 getOrcamentoById:function(id,callback){
 
return db.query("select ID_Orçamento, Forma_Pagamento, ID_Cliente, DATE_FORMAT(Data,'%y-%m-%d') from Orçamento where ID_Orçamento=?",[id],callback);
 },
 addOrcamento:function(Orcamento,callback){
 return db.query("Insert into Orçamento values(?,?,?,?)",[Orcamento.ID_Orçamento,Orcamento.Forma_Pagamento,Orcamento.ID_Cliente,Orcamento.Data],callback);
 },
 deleteOrcamento:function(id,callback){
  return db.query("delete from Orçamento where ID_Orçamento=?",[id],callback);
 },
 updateOrcamento:function(id,Orcamento,callback){
  return db.query("update Orçamento set Forma_Pagamento=?,ID_Cliente=?, Data=? where ID_Orçamento=?",[Orcamento.Forma_Pagamento,Orcamento.ID_Cliente,Orcamento.Data,id],callback);
 }
 
};
 module.exports=Orcamento;
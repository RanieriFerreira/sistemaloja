var express = require('express');
var router = express.Router();
var db=require('./db'); //reference of dbconnection.js
 
var Empresa={
 
getAllEmpresas:function(callback){
 
return db.query("Select * from Empresa",callback);
 
},
 getEmpresaById:function(id,callback){
 
return db.query("select * from Empresa where CNPJ=?",[id],callback);
 },
 addEmpresa:function(Empresa,callback){
 return db.query("Insert into Empresa values(?,?,?,?,?,?)",[Empresa.CNPJ,Empresa.CEP,Empresa.Logradouro,Empresa.Bairro,Empresa.Cidade,Empresa.Estado],callback);
 },
 deleteEmpresa:function(id,callback){
  return db.query("delete from Empresa where CNPJ=?",[id],callback);
 },
 updateEmpresa:function(id,Empresa,callback){
  return db.query("update Empresa set CEP=?,Logradouro=?,Bairro=?,Cidade=?,Estado=? where CNPJ=?",[Empresa.CEP,Empresa.Logradouro,Empresa.Logradouro,Empresa.Cidade,Empresa.Estado,id],callback);
 }
 
};
 module.exports=Empresa;
var express = require('express');
var router = express.Router();
var db=require('./db'); //reference of dbconnection.js
 
var Orcamento={
 
getAllOrcamentoAbertos:function(callback){ 
return db.query("Select * from Orçamento_Aberto",callback); 
},
 addOrcamentoAberto:function(Orcamento,callback){
 return db.query("Insert into Orçamento_Aberto values(?)",[Orcamento.ID_Orçamento],callback);
 },
 deleteOrcamentoAberto:function(id,callback){
  return db.query("delete from Orçamento_Aberto where ID_Orçamento=?",[id],callback);
 }
 
};
 module.exports=Orcamento;
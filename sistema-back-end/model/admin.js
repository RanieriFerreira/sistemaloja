var express = require('express');
var router = express.Router();
var db=require('./db'); //reference of dbconnection.js
 
var Administrador={
 
getAllAdministradores:function(callback){
 
return db.query("Select * from Administrador",callback);
 
},
 getAdministradorById:function(id,callback){
 
return db.query("select * from Administrador where Login=?",[id],callback);
 },
 addAdministrador:function(Administrador,callback){
 return db.query("Insert into Administrador values(?,?,?,?,?)",[Administrador.Login,Administrador.Senha,Administrador.Email,Administrador.Prenome,Administrador.Sobrenome],callback);
 },
 deleteAdministrador:function(id,callback){
  return db.query("delete from Administrador where Login=?",[id],callback);
 },
 updateAdministrador:function(id,Administrador,callback){
  return db.query("update Administrador set senha=?,email=?,prenome=?,sobrenome=? where Login=?",[Administrador.Senha,Administrador.Email,Administrador.Prenome,Administrador.Sobrenome,id],callback);
 }
 
};
 module.exports=Administrador;
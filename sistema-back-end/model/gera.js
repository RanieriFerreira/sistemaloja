var express = require('express');
var router = express.Router();
var db=require('./db'); //reference of dbconnection.js
 
var Gera={
 
getAllGeras:function(callback){
 
return db.query("Select * from Gera",callback);
 
},
 getGeraById:function(id,callback){
 
return db.query("select * from Gera where CNPJ=?",[id],callback);
 },
 addGera:function(Gera,callback){
 return db.query("Insert into Gera values(?,?)",[Gera.CNPJ,Gera.ID_Relatorio],callback);
 },
 deleteGera:function(id,callback){
  return db.query("delete from Gera where ID_Relatorio=?",[id],callback);
 }
 
};
 module.exports=Gera;
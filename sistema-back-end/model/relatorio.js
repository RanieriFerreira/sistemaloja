var express = require('express');
var router = express.Router();
var db=require('./db'); //reference of dbconnection.js
 
var Relatorio={
 
getAllRelatorios:function(callback){
 
return db.query(
    "SELECT o.ID_Orçamento, o.ID_Cliente, o.Data, SUM(p.Quantidade * pr.Valor) AS Total FROM orçamento AS o "
    +"JOIN possui AS p "
    +"ON o.ID_Orçamento=p.ID_Orçamento "
    +"JOIN produto AS pr "
    +"ON pr.ID_Produto = p.ID_Produto "
    +"GROUP BY o.ID_Orçamento ;",callback);
 
},
 getRelatorioById:function(id,callback){
 
return db.query("select * from Relatorio where ID_Relatorio=?",[id],callback);
 },
 addRelatorio:function(Relatorio,callback){
 return db.query("Insert into Relatorio values(?,?,?)",[Relatorio.ID_Relatorio,Relatorio.Faturamento,Relatorio.CNPJ],callback);
 },
 deleteRelatorio:function(id,callback){
  return db.query("delete from Relatorio where ID_Relatorio=?",[id],callback);
 },
 updateRelatorio:function(id,Relatorio,callback){
  return db.query("update Relatorio set Faturamento=?,CNPJ=? where ID_Relatorio=?",[Relatorio.Faturamento,Relatorio.CNPJ,id],callback);
 }
 
};
 module.exports=Relatorio;
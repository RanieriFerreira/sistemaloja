# Projeto para sistema comercial

Esse projeto tem como objetivo criar um sistema funcional para uma empresa de pequeno e médio porte.

---

## Preparação do ambiente

Para trabalhar com React será necessário instalar o nodejs e npm

Para instalar pacote do node e npm poder usar `sudo apt-get install nodejs@8.11.4 npm@5.6.0`
Procure instalar as versões 8.11.4(node) e 5.6.0(npm) )

Instalar o Servidor Mysql 5.7 `sudo apt-get install mysql-server-5.7`

---

## Configurando banco de dados e back

Entrar na pasta sistema-back-end `cd sistema-back-end`

Nos arquivos `model/db.js`, `criaTabela.js` e `criaDB.js` é necessário colocar as configurações do seu banco de dados, para isso basta modificar a variavel `connection` para o seguinte padrão:
    `var connection = mysql.createConnection({
        host     : 'localhost',
        port     : '<porta do banco>',
        user     : '<usuário>',
        password : '<senha>',
        database : 'mds_db'
    });`
(Lembrando que a porta padrão do mysql é a 3306)

Rodar o script de criação do banco `node criaDB.js`

Rodar o script de criação das tabelas `node criaTabelas.js`

E colocar o servidor online `node main.js`


---

## Levantando a aplicação

Após concluir a criação do banco de dados basta entrar na pasta do front `cd ../sistema-front-end/`

Rodar a aplicação `npm start`